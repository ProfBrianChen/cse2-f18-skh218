// Scott Henry
// Homework 8
// November 13, 2018

import java.util.Scanner; //import scanner
   
public class Shuffling{ 
  public static void main(String[] args) { 

  Scanner scan = new Scanner(System.in); //declare scanner

//suits club, heart, spade or diamond 
  String[] suitNames={"C","H","S","D"};    //set of suits
  String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //set of all types of cards
  String[] cards = new String[52]; 
  String[] hand = new String[5]; 
  int numCards = 5; //5 cards in a hand
  int again = 1; //initial var to draw 5 cards
  int index = 51; //general index of 0-51 --> 52 cards/deck
  
  System.out.println("List of all cards in the deck:"); //listing cards in deck
  for (int i=0; i<52; i++){ 
    cards[i]=rankNames[i%13]+suitNames[i/13]; //divide up different suits and numbers
    System.out.print(cards[i]+" "); 
  } 

  System.out.println(); //print statement for organization and display purpose
    
  
   
  shuffle(cards); //call method
  printArray(cards); // call method

  System.out.println(); //print line for display purposes
  while(again == 1){  //while loop if new hand wanted
   hand = getHand(cards,index,numCards); //call methods
   printArray(hand); //print
   index = index - numCards; //subtract the numcards
   System.out.println();
   System.out.println("Enter a 1 if you want another hand drawn"); //prompt to user to continue or not
   again = scan.nextInt(); //input
}  
  } 


  public static void printArray(String [] list){
    
    for(int i=0; i<list.length; i++){ //for loop to display list
      System.out.print(list[i] + " "); //display list
  }
}
  public static String [] shuffle(String [] list){
   
    int i = 0; //i counter
    String [] var = new String[1]; //temporary variable declared
    while( i < 51){
      int rand = (int) (Math.random() * 51 ) ;//initiate random shuffle
      var[0] = list[0]; 
      list[0] = list[rand];
      list[rand] = var[0];
      i++; //incremement
  
  }
   System.out.println(); //print line for organization and display
   System.out.println("Shuffled: "); //print shuffled deck
    return list; //return the array
    
  }
  
  
  public static String [] getHand(String [] list, int index, int numCards){
 
      
    int choice1 = (int) (Math.random()*51); //random card 1 
    
    int choice2 = (int) (Math.random()*51); // random card 2
     
    while (choice1 == choice2) //check to make sure cards not identical
      choice2= (int) (Math.random()*51);
    
    int choice3 = (int) (Math.random()*51); //random card 3
    while (choice3 == choice2 || choice3 == choice1)//check to make sure cards not identical
      choice3= (int) (Math.random()*51);

    int choice4 = (int) (Math.random()*51); //random card 4
    while (choice4 == choice1 || choice4 == choice2 || choice4 == choice3)//check to make sure cards not identical
      choice4= (int) (Math.random()*51);
    
    int choice5 = (int) (Math.random()*51); //random card 5
    while (choice5 == choice1 || choice5 == choice2 || choice5 == choice3 || choice5 == choice4)//check to make sure cards not identical
      choice5= (int) (Math.random()*51);
    
    String[] newSet={list[choice1], list[choice2], list[choice3], list[choice4], list[choice5]}; // new set to show the random hand drawn
    
  System.out.println(); //print line for organization

  System.out.println("Hand: "); //print the Hand
  
  return newSet; //return newSet array
  }
    

  }