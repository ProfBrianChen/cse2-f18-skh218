// Scott Henry
// lab 10

public class Lab10 {
  
  
  public static void main (String [] args) {
    
    int [] list =  {1,2,7,3,6} ;
    
    arraySort2DSelection(list);
    
    System.out.println();
    
    arraySort2DInsertion(list);
    
    
    
    
    
  }
      
    
    public static void arraySort2DSelection(int [] list) {
    
      for (int i = 0; i < list.length - 1; i++){
      int currentMin =list[i];
      int currentMinIndex = i;
      
      for(int j = i +1; j < list.length; j++){
        if(currentMin > list[j]){
          currentMin = list[j];
          currentMinIndex = j;
        }
      }
      if(currentMinIndex != i){
        list[currentMinIndex] = list[i];
        list[i] = currentMin;
      }
    }
    for(int i = 0; i < list.length; i++)
      System.out.print(list[i] + " ");
  
  
     }

   
    public static void arraySort2DInsertion(int [] list) {
      
      for(int i = 1; i < list.length; i++){
      int currentElement = list[i];
      int k;
      for(k = i - 1; k >= 0  &&  list[k] > currentElement; k--){
        list[k +1] = list[k];
      }
      list[k + 1] = currentElement;
    }
     for(int i = 0; i < list.length; i++)
      System.out.print(list[i] + " ");

      
    }
    
      
      
    }
    