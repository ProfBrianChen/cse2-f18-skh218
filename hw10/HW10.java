// Scott Henry
// December 4, 2018
// This program plays tic-tac-toe with Player X and Player O


import java.util.Scanner; //import java scanner

public class HW10 {
 public static void main(String[] args) {
  
   String[] players = {" X "," O "}; // Define players X and O
   String [][] board = showBoard(); //create 2D array matrix AKA tic tac toe board
  
  int result; // define result

  do {
   print(board); //display board, call method
   int[] cell = getCell(board, players[0]); //cell array defined
   
   placeToken(board, cell, players[0]); // call placeToken method - X or O
  
   result = gameStatus(board, players[0]); //result of decision


   if (result == 2) {
    swap(players); //swap the X O in cell
   }

  } while(result == 2);


  print(board); //method call board

  if (result == 0) {
    System.out.println(players[0] + " wins!"); } //print statement if player wins
  else {
    System.out.println("Game resulted in a draw"); } //print statement if a draw results from the game

 }

 public static String[][] showBoard() { //board method
  String[][] toeBoard = new String[3][3]; //define inital 3x3 board
  
  for (int i = 0; i < 3; i++) {
   for (int j = 0; j < 3; j++) {
    toeBoard[i][j] = "   "; //print the blank for the cell
   }
  }
  return toeBoard; //returning the board
 }
 
  public static void print(String[][] index) { //creating the board to have a nice display
  
    System.out.println("\n_____________"); //print the top third divider between rows creating the board
  
  for (int i = 0; i < index.length; i++) {
   for (int j = 0; j < index[i].length; j++) {
    System.out.print("|" + index[i][j]); // printing the bar between cells to create board
   }
   
   System.out.println("|\n_____________"); //print the bottom third divider between rows creating the board
  }
 }
 
 public static int gameStatus(String[][] index, String e) { //method for determing if win or draw
   if (isWin(index, e)) {
     return 0; } // If game results in a Win
   
   else if (isDraw(index)) {
     return 1; } // If game results in a Draw
   
   else {
     return 2; } // to continue with the game
 }

//
 public static boolean isDraw(String[][] index) { //method for determining draw
  for (int i = 0; i < index.length; i++) {
   for (int j = 0; j < index[i].length; j++) {
    if (index[i][j] == "   ") //if blank
     return false; //return draw
   }
  }
  
  return true; //return true (win)
 }
 
 
 public static boolean isWin(String[][] index, String t) { 
  return isHorizontalWin(index, t) || isVerticalWin(index, t) || isDiagonalWin(index, t); //returning the 3 types of wins -- vert/horiz/diag
 }



 public static boolean isHorizontalWin(String[][] index, String t) { //method for horizontal wins
  for (int i = 0; i < index.length; i++) {
   int count = 0; //initialize count
   
   for (int j = 0; j < index[i].length; j++) { //length of array
    if (index[i][j] == t)
     count++; //count if more than one token in a row
   }
   
   if (count == 3) //3 in a row consitutes a win
    return true; //if win occurs
  }
  
  return false; //if no win occurs
 }



 public static boolean isVerticalWin(String[][] m, String t) { //method for vertical wins
  for (int i = 0; i < m.length; i++) {
   int count = 0; //initial count of tokens in a row
   
   for (int j = 0; j < m[i].length; j++) {
    if (m[j][i] == t)
     count++; //increment count if more than one token in a vertical row
   }
   if (count == 3) //3 in a row = win
    return true; //if win
  }
  
  return false; //if no win
 }



 public static boolean isDiagonalWin(String[][] m, String t) { //method for a diagonal win
  int count = 0; //initialize count to 0
  for (int i = 0; i < m.length; i++) {
    if (m[i][i] == t) {
      count++; } //increment count if diagonal in a row
    
    if (count == 3) { //3 in a row consitutes a win
      return true; } //return if a win
  }

  count = 0; //set count back to 0 for opposite diagonal
  for (int i = 0, j = m[i].length - 1; j >= 0 ; j--, i++) {
    if (m[i][j] == t) {
      count++; } //other diagonal count
    
    if (count == 3) {//3 in a row for win
      return true; } //return if win
  }
  
  return false;//return if draw
 }

 public static void swap(String[] p) {
  String temp = p[0]; //initialize temp array var
  p[0] = p[1];
  p[1] = temp; //setting equal to temp var
 }


 public static void placeToken(String[][] m, int[] e, String t) { //placing tokens in desired slot
  m[e[0]][e[1]] = t;
 }


 public static int[] getCell(String[][] m, String t) {

  Scanner input = new Scanner (System.in); //define / initialize scanner 
  int[] cell = new int[2]; // get Cell row and column

  do {
   System.out.print("Please input a row # for entry {0, 1, or 2} for  " + t + ": "); //print statement for entering row to place token
   cell[0] = input.nextInt(); //scanner to get value
   
   System.out.print("Please input a column # for entry {0, 1, or 2} for  " + t + ": "); //print statement for entering column to place token
   cell[1] = input.nextInt(); //scanner to get value

  } while (!isValidCell(m, cell)); //to ensure a correct integer is entered
  
  return cell; //return cell value
 }


 public static boolean isValidCell(String[][] m, int[] cell) { //check to ensure the entered cell is valid
  for (int i = 0; i < cell.length; i++) {
   if (cell[i] < 0 || cell[i] >= 3) { //must be between 0 and 2
    System.out.println("Invalid cell entry, please try again"); //print statement for invalid cell
    
    return false; //boolean value
   }
  }
  
  if (m[cell[0]][cell[1]] != "   ") {
   System.out.println("Row: " + cell[0] + " Column: " + cell[1] + " has been filled"); //error message if cell is already filled
   
   return false; //not successful turn
  }
  return true;  //if a valid cell is entered 
 }



}