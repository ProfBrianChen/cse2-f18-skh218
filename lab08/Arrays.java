// Scott Henry
// Lab 08 
// November 7, 2018
// The program randomly places integers between 0-99 into an array and then counts the number of occurrences of each integer.


public class Arrays {
  public static void main (String[] args){
  
  int [] array1 = new int [100]; //create initial array to generate 100 integers
  int [] array2 = new int [100]; //create initial array to to hold the number of occurrences of each number in the first array
  
  

  
  

  System.out.print("Array 1 holds the following integers: ");
  for (int i = 0; i<= 99; i++){ //for loop to create integers
    array1[i] = (int)(Math.random()*100); // randomly generating integers 0-99 and putting them in array 1
    System.out.print(array1[i]+ " "); //print integers produced in array
  }
    
    for (int index = 0 ; index < array1.length; index++){
      array2[array1[index]] ++; //array 2 set to record the count of number of times each integer occurs
    }
    
    System.out.println();//print new line for organization
    
    for (int j=0; j<= 99; j++){ //for loop for printing the counts
      
      if (array2[j] == 1)
        System.out.println(j + " occurs " + array2[j] + " time"); //print statement if the count is only 1
      else 
        System.out.println(j + " occurs " + array2[j] + " times"); //print statement if count occurs more than once
    }
     
  }
}