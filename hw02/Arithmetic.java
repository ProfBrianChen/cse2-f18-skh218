// Scott Henry
// CSE 2 HW 02
// September 11, 2018
// The purpose of this program is to compute the cost of the items purchased at a department store

public class Arithmetic{
    
    public static void main(String args[]){
      //Inputs
      int numPants = 3; // Number of pairs of pants purchased
      double pantsPrice = 34.98; // Cost per pair of pants
      
      int numShirts = 2; // Number of shirts purchased
      double shirtPrice = 24.99; // Cost per shirt
      
      int numBelts = 1; // Number of belts purchased
      double beltCost = 33.99; // Cost per belt
      
      double paSalesTax = 0.06; // Tax rate
      
      //Calculations
      double totalCostPants = numPants*pantsPrice; // calculation of cost of pants
      double totalCostShirts = numShirts*shirtPrice; // calculation of cost of shirts
      double totalBeltCost = numBelts*beltCost; // calculation of cost of belts
      double preTaxCost = totalCostPants + totalCostShirts + totalBeltCost; // calculation of pre tax cost
      
      double taxPants = Math.round((paSalesTax*totalCostPants)*100)/100.0; // calculation of sales tax on pants
      double taxShirts = Math.round((paSalesTax*totalCostShirts)*100)/100.0; // calculation of sales tax on shirts
      double taxBelts = Math.round((paSalesTax*totalBeltCost)*100)/100.0; // calculation of sales tax on belts
      double totalTax = Math.round((paSalesTax*preTaxCost)*100)/100.0; // calculation of total tax cost
      
      double totalCost = preTaxCost + totalTax; // total transaction cost
      
      System.out.println("Total Cost of Pants: $" + totalCostPants); // Total Price of Pants
      System.out.println("Total Cost of Shirts: $" + totalCostShirts); // Total Price of Shirts
      System.out.println("Total Cost of Belts: $" + totalBeltCost); // Total Price of Belts
      System.out.println("Total Cost Before Tax: $" + preTaxCost); // Total Price Before Tax
      
      System.out.println("Sales Tax for Pants: $" + taxPants); // Sales tax charged for purchase of pants
      System.out.println("Sales Tax for Shirts: $" + taxShirts); // Sales tax charged for purchase of shirts
      System.out.println("Sales Tax for Belts: $" + taxBelts); // Sales tax charged for purchase of belts
      System.out.println("Total Sales Tax: $" + totalTax); // Total Sales Tax for purchase
      System.out.println("Total Transaction Cost: $" + totalCost); // Total transaction cost
      
    }
}