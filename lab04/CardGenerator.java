//Scott Henry
//Lab 04
//September 19 2018
// This program deals with generating random card from a standard deck of 52


public class CardGenerator{
    
    public static void main(String args[]){
      
      int cardNum;  //initial card number
      int cardNum1=0; // new card number
      String suit = ""; //suit
  
      
      cardNum = (int) (Math.random()*51) + 1; //generating random number
      
      if (cardNum>=1 && cardNum <=13)  {
        suit = "diamonds"; //assigning diamonds
        cardNum1=cardNum; // consistent 1-13 diamond
      }
        else if (cardNum>=14 && cardNum<=26){
          suit = "clubs"; //assigning clubs
          cardNum1=cardNum-13; //reassign number to 1-13 clubs
        }
      else if (cardNum>=27 && cardNum<=39){
          suit = "hearts"; //assigning hearts
          cardNum1=cardNum-26; //reassign number to 1-13 hearts
        }
      else if (cardNum>=40 && cardNum<=52){
          suit = "spades"; //assigning spades
          cardNum1=cardNum-39; //reassign 1-13 spades
        }
      
      String cardNum2 = "0"; //initialize and declar new number
      
      switch (cardNum1) { //switches
        case 1: 
           cardNum2 = "Ace"; //assign ace
          break;
        case 2:
          cardNum2 = "2";//assign 2
          break;
        case 3:
          cardNum2 = "3";//assign 3
          break;
        case 4:
          cardNum2 = "4";//assign 4
          break;
        case 5:
          cardNum2 = "5";//assign 5
          break;
        case 6:
          cardNum2 = "6";//assign 6
          break;
        case 7:
          cardNum2 = "7";//assign 7
          break;
        case 8:
          cardNum2 = "8";//assign 8
          break;
        case 9:
          cardNum2 = "9";//assign 9
          break;
        case 10:  
          cardNum2 = "10";//assign 10
          break;
        case 11:
           cardNum2 = "Jack"; //assign jack
          break;
        case 12:
          cardNum2 = "Queen"; //assign queen
          break;
        case 13:
           cardNum2 = "King"; //assign king
          break;
    }
      
      
      
      System.out.println(cardNum); // print out random number 1-52
      System.out.println("Your Card is the " + cardNum2 + " of " + suit); //what card you were dealt
      
      
      
      
    }
}