// Scott Henry
// Lab 07
// Oct. 24, 2018
// This program deals with random sentence generation using methods

import java.util.Scanner; //import scanner
 
public class Methods {

    public static String adjective() {
      int int1 =0;
      String int12 ="0";
      int1 = (int) (Math.random()*10);
      
      switch (int1) { //switches
        case 0: 
           int12 = "big"; //assign big
          break;
        case 1: 
           int12 = "small"; //assign small
          break;
        case 2:
          int12 = "happy";//assign happy
          break;
        case 3:
          int12 = "sad";//assign sad
          break;
        case 4:
          int12 = "long";//assign long
          break;
        case 5:
          int12 = "short";//assign short
          break;
        case 6:
          int12 = "smart";//assign smart
          break;
        case 7:
          int12 = "dumb";//assign dumb
          break;
        case 8:
          int12 = "old";//assign old
          break;
        case 9:
          int12 = "young";//assign young
          break;
      }
      
          return int12;
    }
    
    public static String subjectnoun() {
      int int2 = 0;
      String int22 ="0";
      int2 = (int) (Math.random()*10);
      
      switch (int2) { //switches
        case 0: 
           int22 = "person"; //assign person
          break;
        case 1: 
           int22 = "man"; //assign man
          break;
        case 2:
          int22 = "woman";//assign woman
          break;
        case 3:
          int22 = "son";//assign son
          break;
        case 4:
          int22 = "daughter";//assign daughter
          break;
        case 5:
          int22 = "mother";//assign mother
          break;
        case 6:
          int22 = "father";//assign father
          break;
        case 7:
          int22 = "athelte";//assign athelete
          break;
        case 8:
          int22 = "driver";//assign driver
          break;
        case 9:
          int22 = "worker";//assign worker
          break;
      }
          
          return int22;
    }
    
    public static String verb() {
      int int3 =0;
      String int32 = "0";
      int3 = (int) (Math.random()*10);
      
      switch (int3) { //switches
        case 0: 
           int32 = "ran"; //assign ran
          break;
        case 1: 
           int32 = "jumped"; //assign jumped
          break;
        case 2:
          int32 = "stopped";//assign stopped
          break;
        case 3:
          int32 = "walked";//assign walked
          break;
        case 4:
          int32 = "thought";//assign thought
          break;
        case 5:
          int32 = "studied";//assign studied
          break;
        case 6:
          int32 = "worked";//assign worked
          break;
        case 7:
          int32 = "delivered";//assign delivered
          break;
        case 8:
          int32 = "stumbled";//assign stumbled
          break;
        case 9:
          int32 = "tripped";//assign tripped
          break;
      }
          
          return int32;
      
    }
    
    public static String objectnoun() {
      int int4 =0;
      String int42 = "0";
      int4 = (int) (Math.random()*10);
      
       switch (int4) { //switches
        case 0: 
           int42 = "pond"; //assign pond
          break;
        case 1: 
           int42 = "river"; //assign river
          break;
        case 2:
          int42 = "street";//assign street
          break;
        case 3:
          int42 = "house";//assign house
          break;
        case 4:
          int42 = "park";//assign park
          break;
        case 5:
          int42 = "school";//assign school
          break;
        case 6:
          int42 = "neighborhood";//assign neighborhood
          break;
        case 7:
          int42 = "store";//assign store
          break;
        case 8:
          int42 = "sidewalk";//assign sidewalk
          break;
        case 9:
          int42 = "beach";//assign beach
          break;
       }
          
          return int42;
       
    }
   
    public static void main (String [] args) { 
    int condition = 1;//to continue paragraph
    Scanner myScanner = new Scanner (System.in); //scanner
    
    
    
    do{
    
      int sequences = 0; //number of sequences
      System.out.println("How many action Sequences do you want? ");
      sequences = myScanner.nextInt();  //variable for number of action sentences
      
      if (sequences <0){
        System.out.println("invalid number of action sequences given"); //error if a wrong number is inputted for sequences
        System.exit(0); }
      
      String subject = subjectnoun(); //to ensure the subject remains constant
      //THESIS SENTENCE
      System.out.println("The " + adjective() + " " + adjective() + " " + subject + " " + verb() + " " + "the " + adjective() + " " + objectnoun() + ".");
      
      //ACTION SENTENCE LOOP
      int i =1;
      for (i=1; i <= sequences; i++){ //iterate for how many sequences the user inputs
      System.out.println("This " + subject + " " + verb() +  " to the " + adjective() + " " + objectnoun() + ".");
      }
      
      //ClOSING SENTENCE
     System.out.println("The " + subject + " " + verb() + " her " + objectnoun() + ".");
    
      
    System.out.println("Would you like to print a new sentence? (1 for yes) "); //ask if want to make a new paragraph
    condition = myScanner.nextInt(); // continue adding sentence if user inputs yes
    }
    
    while (condition==1);
    
    System.out.println("Goodbye."); //program ends
    }  
 
}