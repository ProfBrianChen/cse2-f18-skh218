// Scott Henry
// CSE 2 Homework 6
// Oct. 22, 2018
// This program generated encrypted X

import java.util.Scanner;

public class EncryptedX {
  public static void main (String [] args) {
    
   
    int myInteger = 0; //user generated integer value
    int numRows = 0; //num rows for x
    int numColumns = 0; //num columns for x
    String junk = ""; //junk line
    
    
    Scanner myScanner = new Scanner (System.in); //declare scanner
    
    System.out.print("Please enter a positive integer length: "); // enter input integer
    
    while ( myScanner.hasNextInt() == false ){
       System.out.print("Error in value, please enter a positive integer. "); //if not a positive integer
       junk = myScanner.nextLine(); //clear buffer
      
    } 
    
     myInteger = myScanner.nextInt(); //import the length as an int
     junk = myScanner.nextLine(); //clear buffer
    
    
     if(myInteger<1 || myInteger > 100){
     System.out.println("Please enter a valid integer");  //if enter wrong integer
     System.exit(0); //exit if an invalid integer given
    }
     
    for ( numRows = 0; numRows < myInteger; numRows++) { //first for loop for number of rows
        for ( numColumns = 0; numColumns < myInteger; numColumns++) { //nested for loop for number of columns
            if (numRows == numColumns || numRows + numColumns == myInteger - 1) { //for the spaces
                System.out.print(" ");
            } else {
                System.out.print("*"); //for the asterics
            }
        }
        System.out.println(); //line to be printed
    }
  }
}