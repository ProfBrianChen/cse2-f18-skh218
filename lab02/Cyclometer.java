//Scott Henry
// CSE 2 Section 210
// Sept. 5, 2018
//My bicycle cyclometer (meant to measure speed, distance, etc.) records two kinds of data, the time elapsed in seconds, 
//and the number of rotations of the front wheel during that time.

public class Cyclometer {
  
  public static void main (String[] args) {
    int secsTrip1=480; //amount of seconds in trip 1
    int secsTrip2=3220; //amount of seconds in trip 2
    int countsTrip1=1561; // how many counts trip 1
    int countsTrip2=9037; // how many counts trip 2
    
    double wheelDiameter=27.0; //diameter of wheel of the bicycle
    double pi=3.14159; //constant for pi
    double feetPerMile=5280; //amount of feet in 1 mile
    double inchesPerFoot=12; //amount of inches in 1 foot
    double secondsPerMinute=60; //amount of seconds in a foot
    double distanceTrip1, distanceTrip2,totalDistance;
    
    
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");
    System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");
    
   distanceTrip1=countsTrip1*wheelDiameter*pi; // distance calculation trip 1
   distanceTrip1/=inchesPerFoot*feetPerMile; // distance conversion
   distanceTrip2=countsTrip2*wheelDiameter*pi/inchesPerFoot/feetPerMile; // distance calculation trip 2
   totalDistance=distanceTrip1+distanceTrip2; //distance conversion
    
    
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
  }
}
