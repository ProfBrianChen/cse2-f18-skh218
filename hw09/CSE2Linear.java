// Scott Henry
// Homework 9 Program 1
// 11/27/18

import java.util.Arrays; //inport arrays
import java.util.Scanner; //import java scanner

public class CSE2Linear {
  public static void main (String[] args) {
     Scanner scnr = new Scanner (System.in); //declare scanner
     
     int [] scores = new int [15]; // create array of test scores
     
     int i = 0; //counter variable
     
     System.out.println("Please enter 15 test scores in ascending order:"); 
     
      for ( i = 0; i<= 14; i++){ //for loop to enter test scores
        System.out.print("Please enter test score # " + (i+1)); //user prompt
        
        if (scnr.hasNextInt() == false){ //check to ensure the input is an integer
          System.out.println("Error in value, not an integer"); //error message
          System.exit(0); } //quit program
        
        scores[i] = scnr.nextInt(); // User Input test scores 
        
        if (scores[i] > 100 || scores[i] < 0) { //check to ensure the input is between 0-100
          System.out.println("Error in value, out of range 0-100"); //error message
          System.exit(0); } //quit program
       
  }
  
           
           
           
           
      System.out.println(); //Line space for organization
      
      System.out.println("The following scores were entered:"); //display inputted scores
      for (i=0; i<=14; i++)
       System.out.print(scores[i]+ " "); //display Test Scores
      
      System.out.println(); //spacing for organization
     
     
      
        System.out.println("Enter a test score to search for: ");// prompt user for input
        if (scnr.hasNextInt()) { // check to see if int
        
            int x = scnr.nextInt();// save as int
            BinarySearch(scores, x);// method call - linear search
            Scramble(scores);// method call for randomizing array
            
            
            System.out.println("Scrambled:");// prints the scrambled array
            System.out.println(Arrays.toString(scores));// converting to string so it can be printed
           
            
            System.out.print("Enter a grade to search for: ");// Prompt user to input score to search for
            
            if (scnr.hasNextInt()) { // check if value is an integer
                LinearSearch(scores,x);// Linear search if valid
            }
           
            else {
                System.out.println("You didn't enter an integer"); //error message
                System.exit(0);//quit program
            }
        }
        else 
        {
            System.out.println("You didn't enter a grade to seach for"); //error message
            System.exit(0); //quit program
        }
   
  }
    
  public static void LinearSearch(int[] scores, int x) { // method for the linear search
    
        for (int i = 0; i < scores.length; i++)// loop for scores
        {
            if (scores[i] == x)
            {
                System.out.println(x + " was found in the list in the list with " + (i + 1) + " iterations");// print the find
                break;
            }
            else if (scores[i] != x && i == (scores.length - 1)) // if not found
            {
                System.out.println(x + " was not found in the list with " + (i + 1) + " iterations"); //display
            } 
        }
    }
    
    
    public static int[] Scramble(int[] scores) { //method for randomizing the array
    
        for (int i = 0; i < scores.length; i++)
        {
            int x = (int)(Math.random()*scores.length);// generate the random number for 15 scores
            int store = scores[i]; // temporary variable
            while (x != i) 
            {
                scores[i] = scores[x]; //randomizing
                scores[x] = store; //randomizing
                break;
            }
        }
        return scores; //return
    }
    
    
    
    public static void BinarySearch(int[] search, int integer) {
    
        int highIndex = search.length - 1; //high index
        int lowIndex = 0; //low index
        int i = 0; //counter variable
        while (lowIndex <= highIndex)
        {
            i++;
            int midIndex = (highIndex + lowIndex)/2; //average for the mid index of high and low
            if (integer < search[midIndex])
            {
                highIndex = midIndex - 1; //defining high index for search
            }
            else if (integer > search[midIndex])
            {
                lowIndex = midIndex + 1;   
            }
            else if (integer == search[midIndex])
            {
                System.out.println(integer + " was found in the list with " + i + " iterations"); //display for binary search
                break;
            }
        }
        if (lowIndex > highIndex)
        {
            System.out.println(integer + " was not found in the list with " + i + " iterations"); //display for binary search
        }
    }

}