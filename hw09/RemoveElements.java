// Scott Henry
// Homework 9
// Program 2
// November 27, 2018

import java.util.Scanner; //import scanner

public class RemoveElements{
  public static void main(String [] arg){
 
    Scanner scan=new Scanner(System.in); //import scanner

    int num[]=new int[10]; //create array
    int newArray1[]; //first array
    int newArray2[]; //second array
    int index,target; //int variables
    
    String answer=""; //declare and initialize string
 
    do{
   
      System.out.print("Random input 10 ints [0-9]");  //prompt user
      num = randomInput();   //gather input
      String out = "The original array is:";   //prompt user
      out += listArray(num);   //out
      System.out.println(out);  //print out
      
      
      
      System.out.print("Enter the index "); //prompt user   
      index = scan.nextInt();   //import values 
      newArray1 = delete(num,index); 
      String out1="The output array is ";   //prompt user 
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"    
      System.out.println(out1);    //out
      
      
      System.out.print("Enter the target value ");   //prompt user 
      target = scan.nextInt(); 
      newArray2 = remove(num,target);   //newArray2
      String out2="The output array is ";  //prompt user  
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"    
      System.out.println(out2);
   
   
   
   
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   
      answer=scan.next();
    }
 
    while(answer.equals("Y") || answer.equals("y")); //if we want to redo the program
  
  }
 
  
  public static String listArray (int num[]){ //given list arrary
    String out="{";
    
    for(int j=0;j<num.length;j++){  
      if(j>0){    
        out+= ", ";
      }   
      out+=num[j]; 
    }
 
    out+= "} "; 
    return out;
  }

  /// BEGIN METHODS FOR FINISHING THE HOMEWORK AND THE CODE AS A WHOLE
  
  public static int[] randomInput() { // method for getting the random input
  
    
    int random[] = new int [10]; //creating array of 10 slots
    
    for (int i = 0; i < random.length; i++) { 
      int integer = (int)(Math.random()*10); //generating random number
      random[i] = integer; } //setting the randomly generated number into the array
    
    return random; //return the integers
  }
  
  
 
  
  public static int[] remove (int[]array, int index) { // Method for removing the elements
  
    int counter1 = 0; // counter variable
    for (int i = 0; i < array.length; i++){ 
      if (array[i] != index) //if the array doesnt equal y
        counter1++;
      
    }
    
    int array_2[] = new int [counter1]; // create new array with counter index
    int counter2 = 0; // create second counter
    for (int j = 0; j < array.length; j++) { 

      if (array[j] != index) {  //if statement for array
        array_2[counter2] = array[j]; //array for the second equating
        counter2++; //increment
      }
    }
    
    return array_2; //return value
  }

  public static int [] delete (int[]array, int index) { // method for deleting
    
    int [] array_2 = new int[9]; // create array with 9 slots    
    for (int j = 0; j < index; j++)      
      array_2[j] = array[j]; //duplication array   
    
    for (int i = index + 1; i < array.length; i++) {   
      array_2[index] = array[i]; //setting sub index to the array
      index++; //increment
    }
   
    
    return array_2; //return value
  }


} //close class