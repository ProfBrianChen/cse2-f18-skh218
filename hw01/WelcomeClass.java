// Scott Henry
// CSE 2 HW 01
// September 4, 2018

public class WelcomeClass{
    
    public static void main(String args[]){
      
      System.out.println("-----------"); // Beginning of text to be printed
      System.out.println("| WELCOME |");
      System.out.println("-----------");
      System.out.println(" ^  ^  ^  ^  ^  ^");
      System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
      System.out.println("<-S--K--H--2--1--8->"); // my lehigh username
      System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
      System.out.println(" v  v  v  v  v  v");
      System.out.println("Hi, my name's Scott Henry. I'm from Houston, Texas and currently a 5th year IBE Financial Engineering student."); // my tweet like statement
    }
}