//Scott Henry
//HW 04
//September 25, 2018
// This program plays the game of Craps using the switch function

import java.util.Scanner; //Importing scanner feature

public class CrapsSwitch{
    
    public static void main(String args[]){
      
      int diceOne=0; //declare and initilaize dice 1
      int diceTwo=0; //declare and initilaize dice 2
      
      
      Scanner myScanner = new Scanner( System.in );
      System.out.print("Would you like to randomly cast dice? (1 for Yes, 0 for No) "); //prompt user to either randomly cast or input own dice rolls
      int decision = myScanner.nextInt();
      
     if (decision == 1) { //If user chooses to randomly generate a roll
       diceOne = (int) (Math.random()*6) + 1; //generating random number dice 1
       diceTwo = (int) (Math.random()*6) + 1; //generating random number dice 2
     }
      else if (decision == 0) { //if the user manually wants to enter their dice roll
        System.out.print("Enter the result of dice 1: ");
        diceOne = myScanner.nextInt();
        System.out.print("Enter the result of dice 2: ");
        diceTwo = myScanner.nextInt();  
      }
      
      else {
        System.out.println("Please enter either 1 or 0" ); // If the user chooses something different than 1 or 0
        return;
      }
      

      
      String diceOne1 = Integer.toString(diceOne); //Convert dice1 to string
      String diceTwo1 = Integer.toString(diceTwo); //convert dice2 to string
      
      String diceCombo = diceOne1 + diceTwo1; //combining the numbers
      
  
      if ((diceOne>=1 && diceOne <=6) && (diceTwo >=1 && diceTwo <= 6)){ //check to ensure the dice inputs are in the correct range of 1-6
       switch (diceCombo) { //switches
        case "11": 
           System.out.println("You rolled Snake Eyes"); //assign snake eyes
          break;
           case "22": 
           System.out.println("You rolled Hard Four"); //assign hard 4
          break;
           case "33": 
           System.out.println("You rolled Hard Six"); //assign hard 6
          break;
           case "44": 
           System.out.println("You rolled Hard Eight"); //assign hard 8
          break;
           case "55": 
           System.out.println("You rolled Hard Ten"); //assign hard 10
          break;
           case "66": 
           System.out.println("You rolled Boxcars"); //assign boxcars
          break;
           
         case "12":
         case "21":
           System.out.println("You rolled Ace Deuce"); //assign Ace deuce
          break;
         
         case "13":
         case "31":
           System.out.println("You rolled Easy Four"); //assign easy four
          break;
           
         case "14":
         case "41":
         case "23":
         case "32":
           System.out.println("You rolled Fever Five"); //assign fever five
          break;
           
         case "15":
         case "51":
         case "42":
         case "24":
           System.out.println("You rolled Easy Six"); //assign easy six
          break;
           
         case "16":
         case "61":
         case "43":
         case "34":
         case "52":
         case "25":
           System.out.println("You rolled Seven Out"); //assign seven out
          break;
           
         case "62":
         case "26":
         case "53":
         case "35":
           System.out.println("You rolled Easy Eight"); //assign easy eight
          break;
           
         case "54":
         case "45":
         case "63":
         case "36":
           System.out.println("You rolled Nine"); //assign Nine
          break;
           
      
         case "64":
         case "46":
           System.out.println("You rolled Easy Ten"); // assign Easy Ten
          break;
            
         case "65":
         case "56":
           System.out.println("You rolled Yo-levin"); // assign Easy Ten
          break;
       }
      }
      else {
        System.out.println("Please enter valid dice rolls"); //if improper dice rolls are inputted
        return;
      }
     
      System.out.println("Dice 1 roll: " + diceOne); //Print result of dice roll 1
      System.out.println("Dice 2 roll: " + diceTwo); //print result of dice roll 2
    }
}