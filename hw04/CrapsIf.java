//Scott Henry
//HW 04
//September 25, 2018
// This program plays the game of Craps using If statements

import java.util.Scanner; //Importing scanner feature

public class CrapsIf{
    
    public static void main(String args[]){
      
      int diceOne=0; //declare and initilaize dice 1
      int diceTwo=0; //declare and initilaize dice 2
      
      Scanner myScanner = new Scanner( System.in );
      System.out.print("Would you like to randomly cast dice? (1 for Yes, 0 for No) "); //prompt user to either randomly cast or input own dice rolls
      int decision = myScanner.nextInt();
      
     if (decision == 1) { //If user chooses to randomly generate a roll
       diceOne = (int) (Math.random()*6) + 1; //generating random number dice 1
       diceTwo = (int) (Math.random()*6) + 1; //generating random number dice 2
     }
      else if (decision == 0) { //if the user manually wants to enter their dice roll
        System.out.print("Enter the result of dice 1: ");
        diceOne = myScanner.nextInt();
        System.out.print("Enter the result of dice 2: ");
        diceTwo = myScanner.nextInt();  
      }
      
      else {
        System.out.println("Please enter either 1 or 0" ); // If the user chooses something different than 1 or 0
        return;
      }
  
      
     //Translate Data to slang terms
      if (diceOne==1 && diceTwo==1){
        System.out.println("You rolled Snake Eyes"); //print snake eyes
      }
       if (diceOne==2 && diceTwo==2){
        System.out.println("You rolled Hard Four"); //print hard four
      }
       if (diceOne==3 && diceTwo==3){
        System.out.println("You rolled Hard Six"); //print hard six
      }
       if (diceOne==4 && diceTwo==4){
        System.out.println("You rolled Hard Eight"); //print hard eight
      }
       if (diceOne==5 && diceTwo==5){
        System.out.println("You rolled Hard Ten"); //print hard ten
      }
       if (diceOne==6 && diceTwo==6){
        System.out.println("You rolled Boxcars"); //print boxcars
      }
      
      
      else  if ((diceOne == 1 && diceTwo==2) || (diceTwo==1 && diceOne==2)){
        System.out.println("You rolled Ace Deuce");//if statement for Ace Deuce
      }
      
      else  if ((diceOne == 1 && diceTwo==3) || (diceTwo==1 && diceOne==3)){
        System.out.println("You rolled Easy Four"); //if statement for Easy Four
      }
      else  if (((diceOne == 1 && diceTwo==4) || (diceTwo==1 && diceOne==4)) || ((diceOne == 3 && diceTwo==2) || (diceTwo==3 && diceOne==2))){
        System.out.println("You rolled Fever Five"); //if statement for Fever Five
      } 
      else  if (((diceOne == 1 && diceTwo==5) || (diceTwo==1 && diceOne==5)) || ((diceOne == 4 && diceTwo==2) || (diceTwo==4 && diceOne==2))){
        System.out.println("You rolled Easy Six"); //if statement for Easy Six
      }
      else  if (((diceOne == 1 && diceTwo==6) || (diceTwo==1 && diceOne==6)) || ((diceOne == 5 && diceTwo==2) || (diceTwo==5 && diceOne==2)) || ((diceOne == 4 && diceTwo==3) || (diceTwo==4 && diceOne==3))){
        System.out.println("You rolled Seven Out"); //if statement for Seven Out
      }
      else  if (((diceOne == 2 && diceTwo==6) || (diceTwo==2 && diceOne==6)) || ((diceOne == 5 && diceTwo==3) || (diceTwo==5 && diceOne==3))){
        System.out.println("You rolled Easy Eight"); 
      }
      else  if (((diceOne == 3 && diceTwo==6) || (diceTwo==3 && diceOne==6)) || ((diceOne == 4 && diceTwo==5) || (diceTwo==4 && diceOne==5))){
        System.out.println("You rolled Nine");//if statement for Nine
      }
      else  if ((diceOne == 6 && diceTwo==4) || (diceTwo==6 && diceOne==4)){
        System.out.println("You rolled Easy Ten"); //if statement for Easy 10
      }
      else  if ((diceOne == 6 && diceTwo==5) || (diceTwo==6 && diceOne==5)){
        System.out.println("You rolled Yo-levin"); //if statement for yo-levin
      }
    else {
      System.out.println("Please enter valid dice rolls");
      return;// if the user enters invalid dice rolls (i.e. > 6)
    }
    
      System.out.println("Dice 1 roll: " + diceOne); //Print result of dice roll 1
      System.out.println("Dice 2 roll: " + diceTwo); //print result of dice roll 2 
      
    }
}