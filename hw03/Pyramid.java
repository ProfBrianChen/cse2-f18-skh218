// Scott Henry
// CSE 2
// Homework 3 - Program 2
// Sept. 18, 2018
// This program calculates the volume of a pyramid

import java.util.Scanner; //Importing scanner feature

public class Pyramid{
    
    public static void main(String args[]){
      
      Scanner myScanner = new Scanner( System.in );
      System.out.print("The first side of the pyramid is (input length in inches): "); //User input length
      double length = myScanner.nextDouble(); //receiving length
      System.out.print("The second side of the pyramid is (input width in inches): "); //User input width
      double width = myScanner.nextDouble(); //receiving width
      System.out.print("The height of the pyramid is (input height in inches): "); //User input height
      double height = myScanner.nextDouble(); //receiving height
      
      double volume;
      volume = (length*width*height)/3; //calculation of volume
      
      System.out.println("The volume of the pyramid is: " + volume + " cubic inches"); //display of volume
      
    }
}