// Scott Henry
// CSE 2
// Homework 3 - Program 1
// Sept. 18, 2018
// This program accepts inputs and determines how much rainfall occurs over an affected piece of land during a hurricane

import java.util.Scanner; //Importing scanner feature

public class Convert{
    
    public static void main(String args[]){
      
      Scanner myScanner = new Scanner( System.in );
      System.out.print("Enter the affected area in acres: "); //User input affected area in acres
      double acre = myScanner.nextDouble(); //receiving acres affected
      System.out.print("Enter the rainfall in the affected area in inches: "); //User input rainfall in area
      double rainFall = myScanner.nextDouble(); //receiving rainfall
      
      double totalRain;
      totalRain = acre * rainFall * .0015625 * 1.5783E-5; // Calculation of total rainfall and Conversion to Cubic miles
     
      
      System.out.println("The total rainfall is " + totalRain + " cubic miles"); //display of total rainfall
      
      
      
    }
}