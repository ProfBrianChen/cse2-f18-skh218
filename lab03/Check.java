//Scott Henry
// Lab 03 - Check
// September 12, 2018
// This program assists groups who go out to dinner and decide to split the check
// This program will input the amount of people, tip amount, and amount of bill

import java.util.Scanner; //Importing scanner feature

public class Check{
    
    public static void main(String args[]){
      
      Scanner myScanner = new Scanner( System.in );
      System.out.print("Enter the original cost of the check in the form xxx.xx: "); //Input cost of bill
      double checkCost = myScanner.nextDouble(); //receiving check cost
      System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //Input tip amount
      double tipPercent = myScanner.nextDouble(); //receiving tip percent
      tipPercent /= 100; //converting tip percentage to a decimal
      System.out.print("Enter the number of people who went out to dinner: "); //input number of people
      int numPeople = myScanner.nextInt(); // receiving number of people
      
      double totalCost;
      double costPerPerson;
      totalCost = checkCost * (1 + tipPercent); //calculation of total cost
      costPerPerson = totalCost / numPeople; //calculation of cost per person
      
      int dollars, dimes, pennies; //input dimes, pennies, dollars
      dollars=(int)costPerPerson; // converting to integer
      dimes=(int)(costPerPerson * 10) % 10; //number of dimes the person needs to pay
      pennies=(int)(costPerPerson * 100) % 10; //number of pennies person needs to pay
      
      System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);


      
    }
}

  