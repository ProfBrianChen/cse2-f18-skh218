//Scott Henry
//hw 05
// Oct. 11, 2018
// The program prints out twists on the screen

import java.util.Scanner; //import scanner

public class TwistGenerator{
  public static void main (String[]args){
    
    int length = 0; //length of twist
    String junk = ""; //junk var to clear buffer
    String topPattern = "\\  /"; //repeating patter on top
    String midPattern = "  X "; //repeating mid patter
    String botPattern = "/  \\"; //repeating bottom pattern
    String topDisplay=""; //top display
    String midDisplay=""; // mid display
    String botDisplay=""; // bottom display
    
    Scanner myScanner = new Scanner (System.in); //declare scanner
    
    System.out.print("Please enter a positive integer length: "); // enter input length
    

    while ( myScanner.hasNextInt() == false ){
       System.out.print("Error in value, please enter a positive integer length. "); //if not a positive integer
       junk = myScanner.nextLine(); //clear buffer
      
    }  
     
     length = myScanner.nextInt(); //import the length as an int
     junk = myScanner.nextLine(); //clear buffer
    
    int i=0; // for loop counter
    for (i=0; i<length/3; ++i){
      topDisplay += topPattern; //twist top line
      midDisplay += midPattern; //twist middle
      botDisplay += botPattern; //twist bottom
    }
     
     
     System.out.println(topDisplay); //print top line
     System.out.println(midDisplay); //print middle line
     System.out.println(botDisplay); //print bottom line
     
     System.out.println("Length: " + length); //Inputed Length
    
  }
}