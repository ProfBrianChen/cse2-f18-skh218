// Scott Henry
// Lab05
// October 03, 2018
// This program receives the inputs for the current course you are taking

import java.util.Scanner;

public class Lab05 {
  public static void main (String[] args) {
    
    int courseNum = 0; //course number
    String dept = ""; //department
    int timesMet = 0; //times met per week
    int startTime=0; //time the class starts
    String instrName =""; //instructor name
    int numStudents = 0; //number of students in the class
    String junk = ""; //junk variable
    
    Scanner myScanner = new Scanner (System.in);
    
    System.out.print("Enter the course number: "); // enter input
    
    while (myScanner.hasNextInt() == false){
      System.out.print("Error in value, Please enter a new course number. "); //if not an integer
      junk = myScanner.nextLine(); //clearing the buffer
      
    }
    courseNum = myScanner.nextInt();
    junk = myScanner.nextLine(); //clearing the buffer
    
  System.out.print("Enter the number of times the class meets per week: "); // enter input
    while (myScanner.hasNextInt() == false){
      System.out.print("Error in value, Please enter the amount of times the class meets. "); //if not an integer
      junk = myScanner.nextLine(); //clearing the buffer
    }
    timesMet = myScanner.nextInt();
    junk = myScanner.nextLine();//clearing the buffer
    
    System.out.print("Enter the time of day (military) the class starts: "); // enter input
    while (myScanner.hasNextInt() == false){
      System.out.print("Error in value, Please enter a new start time. "); //if not an integer
      junk = myScanner.nextLine(); //clearing the buffer
    }
    startTime = myScanner.nextInt();
    junk = myScanner.nextLine(); //clearing the buffer
    
    System.out.print("Enter the number of students in the class: "); // enter input
    while (myScanner.hasNextInt() == false){
      System.out.print("Error in value, Please enter a new number. "); //if not an integer
      junk = myScanner.nextLine(); //clearing the buffer
    }
    numStudents = myScanner.nextInt();
    junk = myScanner.nextLine(); //clearing the buffer
    
    System.out.print ("Enter the department name: "); // enter input
      dept = myScanner.next();
      junk = myScanner.nextLine(); //clearing the buffer
    System.out.print("Enter the professor's last name: ");
      instrName = myScanner.next();
      junk = myScanner.nextLine(); //clearing the buffer
    
    
    
    
    System.out.println("Course Number: " + courseNum); //print course number
    System.out.println("Department Name: " + dept); //print department name
    System.out.println("Number of Students: " + numStudents); //print number of students
    System.out.println("Start Time: " + startTime); //print start time
    System.out.println("Instructor Last Name: " + instrName); //print instructor last name
    System.out.println("Times met per week: " + timesMet); //print times met per week
    
    
    
    
  }
}