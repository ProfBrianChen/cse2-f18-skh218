//Venmo Activity
//Group F

//Henry, Scott
//Soman, Sebastian
//Hong, Suyeon

//While loop for venmo

import java.util.Scanner;

public class ReportWhile{
  public static void main (String[]args){
    
    Scanner myScanner = new Scanner (System.in);
    System.out.print("How many transactions have you made in September? "); //# trans
    int numTrans = myScanner.nextInt(); //number transactions
    int count = numTrans; //count to see transactions
    double total=0;//total trans amount
    double transAmt =0; // individual trans amt
    double max =0; //max trans
    
    while (count>0){
      System.out.print("How much was your transaction? "); //print trans
      transAmt = myScanner.nextDouble();
      if (transAmt>max) //determine max value
        max=transAmt;
      
      total += transAmt; //total
      count = --count; //counter
   
    }
    
    System.out.println("Total Value of transactions: "+ total); //print result
    System.out.println("Highest Transaction Amount: "+ max); //print result
    System.out.println("Average value of transactions: "+ total/numTrans); //print result
    
    
    
    
  }
  
}