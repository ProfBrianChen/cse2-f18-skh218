// Venmo Variables
// CSE 2 
// September 3, 2018
// Scott Henry / Sebastian Soman / Suyeon Hong 
//Updated

public class VenmoVariables{
    
    public static void main(String args[]){
int routingNum = 38292726; //Routing Number for the bank account
double transAmt = 15.50; //Transaction amount 
int phoneNum = 5555555; // phone number
double bal = 102.78; // Current Account Balance
double newBal = bal + transAmt; // New balance after the transaction
String user = "abc4123"; //username that identifies the person using Venmo
String password = "xyz1234"; // Password for the person using Venmo
final double serviceFee = 2.95; // serivce fee for venmo

      System.out.println("Routing Number:" + " " + routingNum);
      System.out.println("Transaction Amount:"+ " " + transAmt);
      System.out.println("Balance:" + " "+ bal);
      System.out.println("New Balance:"+ " " + newBal);
      System.out.println("Username:" + " "+ user);
      System.out.println("Password:" + " "+ password);
      System.out.println("Phone Number:" + " "+ phoneNum);
       }
}