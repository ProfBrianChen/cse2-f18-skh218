// Scott Henry
// October 30, 2018
// This program has the objective of giving  practice writing methods, manipulating strings and forcing the user to enter good input.

import java.util.Scanner; // import scanner

public class WordTools{
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in); //declare scanner

    char menuOptions = '?'; //menu choice variable initialized
    String mainString = inputString(); //declaring main string calling inputstring
    
    while(menuOptions != 'q'){
      menuOptions = printMenu(); //call to menu
      if(menuOptions == 'c'){
       int n = getNumOfNonWSCharacters(mainString); //determining number of non-whitespace characters
      }
      else if(menuOptions == 'w'){
       int n = getNumOfWords(mainString); //determing amount of words
      }
      else if(menuOptions == 'f'){ //word find function
        System.out.print("Enter the word or phrase to be found : ");
        String findText = myScanner.next();
       int n = findText(mainString,findText); //call to main
      }
      
      else if(menuOptions == 'r'){ //replacing !s
       String output = replaceExclamation(mainString);
       System.out.println("Edited string is :\n" + output); //display edited string
      }
      
      else if(menuOptions == 's'){ //shortening spaces
       String output = shortenSpace(mainString);
       System.out.println("Edited string is :\n"+output); //display new text
      }
      
      else if(menuOptions == 'q')
       System.out.println("Goodbye."); //if choosing to end the program
      
      else
        System.out.println("Invalid input. Please enter a valid menu option. "); //output if a wrong menu option is chosen
      
    }
  }
    
  public static String inputString(){
  
    Scanner myScanner = new Scanner(System.in); //declare scanner
    
    String mainString = ""; //declare and initiate the main String
    System.out.print("Enter a sample text: "); //prompt user to enter a text
    mainString = myScanner.nextLine(); //text is gathered
    
    
    System.out.print("You entered: "); //repeat back to user what the inputted text was
    System.out.println(mainString); //display text
    
    return mainString;
  }
  
  public static char printMenu(){
  
    Scanner myScanner = new Scanner(System.in); //declare scanner
    char menuOptions;
    System.out.println("MENU:"); //the following shows all of the menu choices as displayed in HW description
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.print("Choose an option: ");
    
    menuOptions = myScanner.next().charAt(0);//take next char for menu options
    
    return menuOptions; //return value 
  }
 
  public static int getNumOfNonWSCharacters(String mainString){
  
  char characters[] = mainString.toCharArray();
  int numOfNonWSChars = 0; //whitespace characters
  
  for(int i=0; i < mainString.length(); i++){
    if(characters[i]==' '){
      continue;
    }
    
    else{
      numOfNonWSChars++;
    }
  }
  
  System.out.println("The number of non-whitespace characters in the string : " + numOfNonWSChars);
   
  return numOfNonWSChars;
 }
 
  
  public static int getNumOfWords(String mainString){
   
   char characters[] = mainString.toCharArray(); //
   
   boolean condition = true;//set condition initially equal to true for loop to run
   int numOfWords = 0; //decalare and initialize num0fwords
   
  
    for(int i=0; i < mainString.length(); i++){
    
      if(condition==true && characters[i] != ' '){
        condition = false; //set variable to false
        numOfWords++; //incrememnt number of words count
        continue;   
      }
    
    else{
      if(characters[i] == ' ' && condition==false){
        condition = true; //setting the condition back to true
      }
      
      else{
        continue;
      }
    }
  }
  
    System.out.println("The number of words in the string : " + numOfWords); //print number of words in the entered string 
    return numOfWords;
 }
 
  public static int findText(String mainString,String findText){
  
  int count  = mainString.replaceAll("[^" + findText + "]", "").length() / findText.length(); //replace the white space characters
  
  if(count > 0){
    System.out.println(findText + " occurs "+ count +" times in "+ mainString); // display for amount of times a find occurs
  }
  else{
    System.out.println(findText+" didn't occur in "+mainString); //display if the desired find does not exist in the given string
  }
   return count;
 }
 
  public static String replaceExclamation(String mainString){
 
  String output = ""; //decalare and initialize output string
  char characters[] = mainString.toCharArray();
  
  for(int i=0; i<mainString.length(); i++){
    if(characters[i]=='!'){ //if statement if ! is present
      output = output + '.';
    }
    else{
      output = output + characters[i]; //output + the characters
    }
  }
  return output;
 }
 
  public static String shortenSpace(String mainString){ //method for shoretning string
   
   String output = mainString.replaceAll("2", " "); //new string for shortened spaces
  return output;
 }
}
